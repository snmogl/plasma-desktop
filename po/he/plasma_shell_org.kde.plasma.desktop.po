# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org.kde.plasma.desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-25 01:34+0000\n"
"PO-Revision-Date: 2023-11-23 11:38+0200\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.3\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "בשימוש כרגע"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr "מעבר לפעילות הזאת"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"להציג גם\n"
"בפעילות זו"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "הגדרות"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "עצירת פעילות"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "פעילויות עצורות:"

#: contents/activitymanager/ActivityManager.qml:122
msgid "Create activity…"
msgstr "יצירת פעילות…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "פעילויות"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "הגדרת פעילות"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "מחק"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "אירעה שגיאה בטעינת %1, סליחה!"

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "העתקה ללוח הגזירים"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "הצגת פרטי השגיאה…"

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "פתיחת %1"

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr "על אודות"

#: contents/configuration/AboutPlugin.qml:49
msgid "Send an email to %1"
msgstr "שליחת דוא״ל אל %1"

#: contents/configuration/AboutPlugin.qml:63
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "פתיחת האתר %1"

#: contents/configuration/AboutPlugin.qml:134
msgid "Copyright"
msgstr "זכויות יוצרים"

#: contents/configuration/AboutPlugin.qml:152 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "רישיון:"

#: contents/configuration/AboutPlugin.qml:155
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "הצגת טקסט הרישיון"

#: contents/configuration/AboutPlugin.qml:169
msgid "Authors"
msgstr "יוצרים"

#: contents/configuration/AboutPlugin.qml:180
msgid "Credits"
msgstr "תודות"

#: contents/configuration/AboutPlugin.qml:192
msgid "Translators"
msgstr "מתרגמים"

#: contents/configuration/AboutPlugin.qml:209
msgid "Report a Bug…"
msgstr "דיווח על תקלה…"

#: contents/configuration/AppletConfiguration.qml:76
msgid "Keyboard Shortcuts"
msgstr "מקשי קיצור במקלדת"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "החלת הגדרות"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr "ההגדרות של המודול הנוכחי השתנו. לשמור אותן?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "אישור"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "החלה"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "ביטול"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "פתיחת עמוד הגדרות"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "לחצן שמאלי"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "לחצן ימני"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "לחצן אמצעי"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "לחצן חזרה"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "כפתור הרצה"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "גלילה אנכית"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "גלילה אופקית"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "על אודות"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "הוספת פעולה"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr "שינוי פריסה הוגבל על ידי הנהלת המערכת"

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "פריסה:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper type:"
msgstr "סוג תמונת רקע:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr "הורדת תוספים חדשים…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr "יש להחיל שינויים בפריסה לפני שניתן לבצע שינויים אחרים"

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
msgid "Apply Now"
msgstr "החלה כעת"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr "קיצורי מקשים"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "קיצור מקשים זה יפעיל את היישומון כאילו שלחצו עליו."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "רקע"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "פעולות עכבר"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "קלט כאן"

#: contents/configuration/PanelConfiguration.qml:91
msgid "Panel Settings"
msgstr "הגדרות לוח"

#: contents/configuration/PanelConfiguration.qml:97
msgid "Add Spacer"
msgstr "הוספת מפריד"

#: contents/configuration/PanelConfiguration.qml:100
msgid "Add spacer widget to the panel"
msgstr "הוספת יישומון מרווח ללוח"

#: contents/configuration/PanelConfiguration.qml:107
msgid "Add Widgets…"
msgstr "הוספת יישומונים…"

#: contents/configuration/PanelConfiguration.qml:110
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr "פתיחת בורר היישומונים כדי לגרור יישומונים ללוח"

#: contents/configuration/PanelConfiguration.qml:139
msgid "Position"
msgstr "מקום"

#: contents/configuration/PanelConfiguration.qml:143
#: contents/configuration/PanelConfiguration.qml:254
msgid "Top"
msgstr "למעלה"

#: contents/configuration/PanelConfiguration.qml:144
#: contents/configuration/PanelConfiguration.qml:256
msgid "Right"
msgstr "ימין"

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:254
msgid "Left"
msgstr "שמאל"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:256
msgid "Bottom"
msgstr "למטה"

#: contents/configuration/PanelConfiguration.qml:163
msgid "Set Position..."
msgstr "הגדרת מקום…"

#: contents/configuration/PanelConfiguration.qml:190
msgid "Alignment"
msgstr "יישור"

#: contents/configuration/PanelConfiguration.qml:255
msgid "Center"
msgstr "במרכז"

#: contents/configuration/PanelConfiguration.qml:278
msgid "Length"
msgstr "אורך"

#: contents/configuration/PanelConfiguration.qml:296
msgid "Fill height"
msgstr "מילוי הגובה"

#: contents/configuration/PanelConfiguration.qml:296
msgid "Fill width"
msgstr "מילוי הרוחב"

#: contents/configuration/PanelConfiguration.qml:297
msgid "Fit content"
msgstr "התאמת תוכן"

#: contents/configuration/PanelConfiguration.qml:298
msgid "Custom"
msgstr "התאמה אישית"

#: contents/configuration/PanelConfiguration.qml:328
msgid "Visibility"
msgstr "חשיפה"

#: contents/configuration/PanelConfiguration.qml:340
msgid "Always visible"
msgstr "גלוי תמיד"

#: contents/configuration/PanelConfiguration.qml:341
msgid "Auto hide"
msgstr "הסתרה אוטומטית"

#: contents/configuration/PanelConfiguration.qml:342
msgid "Dodge windows"
msgstr "חמיקת חלונות"

#: contents/configuration/PanelConfiguration.qml:343
msgid "Windows Go Below"
msgstr "חלונות צוללים מתחת"

#: contents/configuration/PanelConfiguration.qml:386
msgid "Opacity"
msgstr "אטימות"

#: contents/configuration/PanelConfiguration.qml:399
msgid "Adaptive"
msgstr "מסתגל"

#: contents/configuration/PanelConfiguration.qml:400
msgid "Opaque"
msgstr "אטום"

#: contents/configuration/PanelConfiguration.qml:401
msgid "Translucent"
msgstr "שקוף למחצה"

#: contents/configuration/PanelConfiguration.qml:425
msgid "Style"
msgstr "סגנון"

#: contents/configuration/PanelConfiguration.qml:436
msgid "Floating"
msgstr "צף"

#: contents/configuration/PanelConfiguration.qml:510
msgid "Panel Width:"
msgstr "רוחב לוח:"

#: contents/configuration/PanelConfiguration.qml:510
msgid "Height:"
msgstr "גובה:"

#: contents/configuration/PanelConfiguration.qml:544
msgid "Focus shortcut:"
msgstr "קיצור מקשים למיקוד:"

#: contents/configuration/PanelConfiguration.qml:554
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "יש ללחוץ על קיצור מקשים זה כדי להעביר את המיקוד ללוח"

#: contents/configuration/PanelConfiguration.qml:570
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "מחיקת לוח"

#: contents/configuration/PanelConfiguration.qml:573
msgid "Remove this panel; this action is undo-able"
msgstr "הסרת הלוח הזה, זאת פעולה בלתי הפיכה"

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr "יש לגרור כדי לשנות את הגובה המרבי."

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr "אפשר לגרור כדי לשנות את הרוחב המרבי."

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr "לחיצה כפולה לאיפוס."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr "אפשר לגרור כדי לשנות את הגובה המזערי."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr "אפשר לגרור כדי לשנות את הרוחב המזערי."

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"יש לגרור כדי לשנות את המיקום בקצה המסך הזה.\n"
"לחיצה כפולה לאיפוס."

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "ניהול לוחות ושולחנות עבודה"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "אפשר לגרור לוחות ושולחנות עבודה סביב כדי להזיז אותם למסכים אחרים."

#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "סגירה"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "החלפה עם שולחן עבודה במסך %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "העברה למסך %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Desktop"
msgstr "הסרת שולחן עבודה"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "הסרת לוח"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr "%1 (עיקרי)"

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "יישומונים חלופיים"

#: contents/explorer/AppletDelegate.qml:42
msgid "Unsupported Widget"
msgstr "יישומון לא נתמך"

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "ביטול הסרה"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "הסרת יישומון"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "מחבר:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "דוא״ל:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "הסרה"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "כל היישומונים"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "יישומונים"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "הורדת יישומונים חדשים…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "קטגוריות"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr "אף יישומון לא עונה על תנאי החיפוש"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr "אין יישומונים זמינים"

#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "הגדלה"

#~ msgid "Make this panel as tall as possible"
#~ msgstr "להגביה את הלוח הזה ככל הניתן"

#~ msgid "Make this panel as wide as possible"
#~ msgstr "הרחבת הלוח הזה ככל הניתן"

#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "מחיקה"

#~ msgid ""
#~ "Aligns a non-maximized panel to the top; no effect when panel is maximized"
#~ msgstr ""
#~ "מיישר את הלוח הלא מוגדל לחלק העליון, אין השפעה כאשר הלוח מוגדל לגמרי"

#~ msgid ""
#~ "Aligns a non-maximized panel to the left; no effect when panel is "
#~ "maximized"
#~ msgstr "מיישר את הלוח הלא מוגדל לשמאל, אין השפעה כאשר הלוח מוגדל לגמרי"

#~ msgid ""
#~ "Aligns a non-maximized panel to the center; no effect when panel is "
#~ "maximized"
#~ msgstr "מיישר את הלוח הלא מוגדל למרכז, אין השפעה כאשר הלוח מוגדל לגמרי"

#~ msgid ""
#~ "Aligns a non-maximized panel to the bottom; no effect when panel is "
#~ "maximized"
#~ msgstr ""
#~ "מיישר את הלוח הלא מוגדל לחלק התחתון, אין השפעה כאשר הלוח מוגדל לגמרי"

#~ msgid ""
#~ "Aligns a non-maximized panel to the right; no effect when panel is "
#~ "maximized"
#~ msgstr "מיישר את הלוח הלא מוגדל לימין, אין השפעה כאשר הלוח מוגדל לגמרי"

#~ msgid ""
#~ "Panel is hidden, but reveals itself when the cursor touches the panel's "
#~ "screen edge"
#~ msgstr "הלוח מוסתר אך הוא יחשוף את עצמו עם נגיעת הסמן בקצה מסך הלוח"

#~ msgid ""
#~ "Panel is opaque when any windows are touching it, and translucent at "
#~ "other times"
#~ msgstr "הלוח אטום כשלא נוגע בו אף חלון ושקוף למחצה במקרים אחרים"

#~ msgid "Floating:"
#~ msgstr "ציפה:"

#~ msgid "Panel visibly floats away from its screen edge"
#~ msgstr "הלוח צף הרחק מקצה המסך"

#~ msgid "Attached"
#~ msgstr "מוצמד"

#~ msgid "Panel is attached to its screen edge"
#~ msgstr "הלוח מוצמד לקצה המסך"

#~ msgid "More Options…"
#~ msgstr "אפשרויות נוספות…"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Drag to move"
#~ msgstr "גרירה להעברה"

#~ msgctxt "@info:tooltip"
#~ msgid "Use arrow keys to move the panel"
#~ msgstr "להשתמש במקשי החצים כדי להעביר את הלוח"

#~ msgid "Switch"
#~ msgstr "החלף"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "חלונות נמצאים מתחת"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "חלונות יכולים לכסות"

#, fuzzy
#~| msgid "Search..."
#~ msgid "Search…"
#~ msgstr "חפש..."

#~ msgid "Screen Edge"
#~ msgstr "קצה המסך"

#~ msgid "Width"
#~ msgstr "רוחב"

#, fuzzy
#~| msgid "Layout cannot be changed whilst widgets are locked"
#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "אי אפשר לשנות פריסה כשהיישומונים נעולים"

#~ msgid "Lock Widgets"
#~ msgstr "נעל יישומונים"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "הקיצור יפעיל את היישומון: הוא ייתן למקלדת להתמקד בו, ואם ליישומון יש חלון "
#~ "קופץ (כגון תפריט התחלה), החלון הקופץ יפתח."
