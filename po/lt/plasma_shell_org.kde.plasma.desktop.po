# Lithuanian translations for l package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the l package.
#
# Automatically generated, 2014.
# Liudas Ališauskas <liudas@aksioma.lt>, 2014, 2015.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: l 10n\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-25 01:34+0000\n"
"PO-Revision-Date: 2023-02-04 20:05+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.2.2\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Šiuo metu naudojama"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Perkelti į\n"
"šią veiklą"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Taip pat rodyti\n"
"šioje veikloje"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Konfigūruoti"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Stabdyti veiklą"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Sustabdytos veiklos:"

#: contents/activitymanager/ActivityManager.qml:122
msgid "Create activity…"
msgstr "Sukurti veiklą…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Veiklos"

#: contents/activitymanager/StoppedActivityItem.qml:137
msgid "Configure activity"
msgstr "Konfigūruoti veiklą"

#: contents/activitymanager/StoppedActivityItem.qml:154
msgid "Delete"
msgstr "Ištrinti"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Atleiskite! Įkeliant %1, įvyko klaida."

#: contents/applet/AppletError.qml:166
msgid "Copy to Clipboard"
msgstr "Kopijuoti į iškarpinę"

#: contents/applet/AppletError.qml:189
msgid "View Error Details…"
msgstr "Rodyti išsamiau apie klaidą…"

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "Atverti %1"

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr "Apie"

#: contents/configuration/AboutPlugin.qml:49
msgid "Send an email to %1"
msgstr "Siųsti el. laišką, adresu %1"

#: contents/configuration/AboutPlugin.qml:63
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Atverti internetinę svetainę %1"

#: contents/configuration/AboutPlugin.qml:134
msgid "Copyright"
msgstr "Autorių teisės"

#: contents/configuration/AboutPlugin.qml:152 contents/explorer/Tooltip.qml:92
msgid "License:"
msgstr "Licencija:"

#: contents/configuration/AboutPlugin.qml:155
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Rodyti licencijos tekstą"

#: contents/configuration/AboutPlugin.qml:169
msgid "Authors"
msgstr "Autoriai"

#: contents/configuration/AboutPlugin.qml:180
msgid "Credits"
msgstr "Padėkos"

#: contents/configuration/AboutPlugin.qml:192
msgid "Translators"
msgstr "Vertėjai"

#: contents/configuration/AboutPlugin.qml:209
msgid "Report a Bug…"
msgstr "Pranešti apie klaidą…"

#: contents/configuration/AppletConfiguration.qml:76
msgid "Keyboard Shortcuts"
msgstr "Spartieji klavišai"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "Taikyti nuostatas"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Šio modulio nuostatos pasikeitė. Ar norite šiuos pakeitimus taikyti ar juos "
"atmesti?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "Gerai"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "Taikyti"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "Atsisakyti"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Atverti konfigūracijos puslapį"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Kairysis mygtukas"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Dešinysis mygtukas"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Vidurinysis mygtukas"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Perėjimo atgal mygtukas"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Perėjimo pirmyn mygtukas"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Vertikali slinktis"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Horizontali slinktis"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Lyg2"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Vald"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Apie"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Pridėti veiksmą"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr "Sistemos administratorius apribojo išdėstymo pakeitimus"

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "Išdėstymas:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper type:"
msgstr "Darbalaukio fono tipas:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr "Gauti naujus papildinius…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr ""
"Prieš atliekant kitus pakeitimus, turi būti įrašyti išdėstymo pakeitimai"

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
msgid "Apply Now"
msgstr "Taikyti dabar"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr "Spartieji klavišai"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""
"Šis spartusis klavišas aktyvuos programėlę taip, lyg ant jos buvo spustelėta."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Darbalaukio fonas"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Pelės veiksmai"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Įveskite čia"

#: contents/configuration/PanelConfiguration.qml:91
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "Taikyti nuostatas"

#: contents/configuration/PanelConfiguration.qml:97
msgid "Add Spacer"
msgstr "Pridėti skirtuką"

#: contents/configuration/PanelConfiguration.qml:100
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:107
msgid "Add Widgets…"
msgstr "Pridėti valdiklius…"

#: contents/configuration/PanelConfiguration.qml:110
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:139
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:143
#: contents/configuration/PanelConfiguration.qml:254
msgid "Top"
msgstr "Viršuje"

#: contents/configuration/PanelConfiguration.qml:144
#: contents/configuration/PanelConfiguration.qml:256
msgid "Right"
msgstr "Dešinėje"

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:254
msgid "Left"
msgstr "Kairėje"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:256
msgid "Bottom"
msgstr "Apačioje"

#: contents/configuration/PanelConfiguration.qml:163
msgid "Set Position..."
msgstr ""

#: contents/configuration/PanelConfiguration.qml:190
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment"
msgstr "Skydelio lygiavimas"

#: contents/configuration/PanelConfiguration.qml:255
msgid "Center"
msgstr "Centre"

#: contents/configuration/PanelConfiguration.qml:278
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Panel height:"
msgid "Fill height"
msgstr "Skydelio aukštis:"

#: contents/configuration/PanelConfiguration.qml:296
#, fuzzy
#| msgid "Panel width:"
msgid "Fill width"
msgstr "Skydelio plotis:"

#: contents/configuration/PanelConfiguration.qml:297
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:298
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:328
#, fuzzy
#| msgid "Visibility"
msgid "Visibility"
msgstr "Matomumas"

#: contents/configuration/PanelConfiguration.qml:340
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Visada matomas"

#: contents/configuration/PanelConfiguration.qml:341
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto hide"
msgstr "Automatiškai slėpti"

#: contents/configuration/PanelConfiguration.qml:342
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:343
#, fuzzy
#| msgid "Windows Go Below"
msgid "Windows Go Below"
msgstr "Langai palenda"

#: contents/configuration/PanelConfiguration.qml:386
#, fuzzy
#| msgid "Opacity"
msgid "Opacity"
msgstr "Nepermatomumas"

#: contents/configuration/PanelConfiguration.qml:399
msgid "Adaptive"
msgstr "Prisitaikantis"

#: contents/configuration/PanelConfiguration.qml:400
#, fuzzy
#| msgid "Opaque"
msgid "Opaque"
msgstr "Nepermatomas"

#: contents/configuration/PanelConfiguration.qml:401
#, fuzzy
#| msgid "Translucent"
msgid "Translucent"
msgstr "Pusiau permatomas"

#: contents/configuration/PanelConfiguration.qml:425
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:436
#, fuzzy
#| msgid "Floating Panel"
msgid "Floating"
msgstr "Slankus skydelis"

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Panel width:"
msgid "Panel Width:"
msgstr "Skydelio plotis:"

#: contents/configuration/PanelConfiguration.qml:510
#, fuzzy
#| msgid "Height"
msgid "Height:"
msgstr "Aukštis"

#: contents/configuration/PanelConfiguration.qml:544
#, fuzzy
#| msgid "Shortcut"
msgid "Focus shortcut:"
msgstr "Spartusis klavišas"

#: contents/configuration/PanelConfiguration.qml:554
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Paspauskite šį spartųjį klavišą norėdami perkelti fokusą į skydelį"

#: contents/configuration/PanelConfiguration.qml:570
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Šalinti skydelį"

#: contents/configuration/PanelConfiguration.qml:573
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr "Tempkite, norėdami keisti didžiausią aukštį."

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr "Tempkite, norėdami keisti didžiausią plotį."

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr "Spustelėkite du kartus, norėdami atstatyti."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr "Tempkite, norėdami keisti mažiausią aukštį."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr "Tempkite, norėdami keisti mažiausią plotį."

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Tempkite, norėdami keisti poziciją šiame ekrano krašte.\n"
"Spustelėkite du kartus, norėdami atstatyti."

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Skydelių ir darbalaukių tvarkymas"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"Galite tempti pirmyn atgal skydelius ir darbalaukius, kad perkeltumėte juos "
"į skirtingus ekranus."

#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Užverti"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "Sukeisti su darbalaukiu, esančiu ekrane %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "Perkelti į ekraną %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "Šalinti skydelį"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "Šalinti skydelį"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr "%1 (pirminis)"

#: contents/explorer/AppletAlternatives.qml:65
msgid "Alternative Widgets"
msgstr "Alternatyvūs valdikliai"

#: contents/explorer/AppletDelegate.qml:42
msgid "Unsupported Widget"
msgstr ""

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "Atšaukti pašalinimą"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "Pašalinti valdiklį"

#: contents/explorer/Tooltip.qml:101
msgid "Author:"
msgstr "Autorius:"

#: contents/explorer/Tooltip.qml:109
msgid "Email:"
msgstr "El. paštas:"

#: contents/explorer/Tooltip.qml:128
msgid "Uninstall"
msgstr "Pašalinti"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:192
msgid "All Widgets"
msgstr "Visi valdikliai"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Valdikliai"

#: contents/explorer/WidgetExplorer.qml:152
msgid "Get New Widgets…"
msgstr "Gauti naujus valdiklius…"

#: contents/explorer/WidgetExplorer.qml:203
msgid "Categories"
msgstr "Kategorijos"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets matched the search terms"
msgstr "Joks valdiklis neatitiko paieškos žodžių"

#: contents/explorer/WidgetExplorer.qml:285
msgid "No widgets available"
msgstr "Nėra jokių prieinamų valdiklių"

#, fuzzy
#~| msgid "Maximize Panel"
#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "Išskleisti skydelį"

#, fuzzy
#~| msgid "Delete"
#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "Ištrinti"

#, fuzzy
#~| msgid "Floating Panel"
#~ msgid "Floating:"
#~ msgstr "Slankus skydelis"

#~ msgid "More Options…"
#~ msgstr "Daugiau parinkčių…"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Drag to move"
#~ msgstr "Tempkite, norėdami perkelti"

#~ msgctxt "@info:tooltip"
#~ msgid "Use arrow keys to move the panel"
#~ msgstr "Norėdami perkelti skydelį, naudokite rodyklių klavišus"

#~ msgid "Switch"
#~ msgstr "Perjungti"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Langai palenda"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Langai gali uždengti"

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (išjungtas)"

#~ msgid "Appearance"
#~ msgstr "Išvaizda"

#~ msgid "Search…"
#~ msgstr "Ieškoti…"

#~ msgid "Screen Edge"
#~ msgstr "Ekrano kraštas"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Spustelėkite ir tempkite mygtuką į ekrano kraštą, norėdami perkelti ten "
#~ "skydelį."

#~ msgid "Width"
#~ msgstr "Plotis"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Spustelėkite ir tempkite mygtuką, norėdami keisti skydelio dydį."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Kol valdikliai užrakinti, negalite keisti išdėstymo"

#~ msgid "Lock Widgets"
#~ msgstr "Užrakinti valdiklius"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Šis trumpinys aktyvuos valdiklį: sufokusuos klaviatūrą į save ir jei "
#~ "valdiklis turi iššokantį meniu (tokį kaip paleidimo meniu), meniu bus "
#~ "atvertas."

#~ msgid "Stop"
#~ msgstr "Stabdyti"

#~ msgid "Activity name:"
#~ msgstr "Veiklos pavadinimas:"

#~ msgid "Create"
#~ msgstr "Sukurti"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Ar tikrai norite ištrinti šią veiklą?"
