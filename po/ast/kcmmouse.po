# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-06 01:34+0000\n"
"PO-Revision-Date: 2023-11-04 18:38+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:100
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:105
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:116
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Nun ye posible tolos cambeos. Mira los rexistros pa consiguir más "
"información. Reanicia esti módulu de configuración ya volvi tentalo."

#: kcm/libinput/libinput_config.cpp:136
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:158
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:182
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:184
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:74
#, kde-format
msgid "Device:"
msgstr "Preséu:"

#: kcm/libinput/main.qml:98 kcm/libinput/main_deviceless.qml:50
#, kde-format
msgid "General:"
msgstr "Xeneral:"

#: kcm/libinput/main.qml:99
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:119
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:124 kcm/libinput/main_deviceless.qml:51
#, kde-format
msgid "Left handed mode"
msgstr ""

#: kcm/libinput/main.qml:144 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:149 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:169 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:178 kcm/libinput/main_deviceless.qml:106
#, kde-format
msgid "Pointer speed:"
msgstr "Velocidá del punteru:"

#: kcm/libinput/main.qml:277 kcm/libinput/main_deviceless.qml:138
#, kde-format
msgid "Pointer acceleration:"
msgstr "Aceleración del punteru:"

#: kcm/libinput/main.qml:308 kcm/libinput/main_deviceless.qml:169
#, kde-format
msgid "None"
msgstr "Nenguna"

#: kcm/libinput/main.qml:312 kcm/libinput/main_deviceless.qml:173
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:318 kcm/libinput/main_deviceless.qml:179
#, kde-format
msgid "Standard"
msgstr "Estándar"

#: kcm/libinput/main.qml:322 kcm/libinput/main_deviceless.qml:183
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:334 kcm/libinput/main_deviceless.qml:195
#, kde-format
msgid "Scrolling:"
msgstr "Desplazamientu:"

#: kcm/libinput/main.qml:335 kcm/libinput/main_deviceless.qml:196
#, kde-format
msgid "Invert scroll direction"
msgstr ""

#: kcm/libinput/main.qml:351 kcm/libinput/main_deviceless.qml:212
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:356
#, kde-format
msgid "Scrolling speed:"
msgstr ""

#: kcm/libinput/main.qml:406
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Despacio"

#: kcm/libinput/main.qml:412
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Rápido"

#: kcm/libinput/main.qml:422
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:460
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:490
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:491
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:495
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:512
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr ""

#: kcm/libinput/main.qml:513
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:542
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""
