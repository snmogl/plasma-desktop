# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# gummi <gudmundure@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2023-03-12 22:15+0000\n"
"Last-Translator: gummi <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Aðal (sjálfgefið)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Skammsnið"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Langsnið"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Umsnúið skammsnið"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Umsnúið langsnið"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Fylgja virka skjánum"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "Passa við úttak"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Úttak passi við teikniborð"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Sérsniðin stærð"

#: ui/main.qml:31
#, kde-format
msgid "No drawing tablets found."
msgstr "Engin teikniborð fundust"

#: ui/main.qml:39
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Tæki:"

#: ui/main.qml:77
#, kde-format
msgid "Target display:"
msgstr "Markskjár:"

#: ui/main.qml:96
#, kde-format
msgid "Orientation:"
msgstr "Stefna:"

#: ui/main.qml:108
#, kde-format
msgid "Left-handed mode:"
msgstr "Snið fyrir örvhenta:"

#: ui/main.qml:118
#, kde-format
msgid "Area:"
msgstr "Svæði:"

#: ui/main.qml:206
#, kde-format
msgid "Resize the tablet area"
msgstr "Endurstilla stærð teiknisvæðis"

#: ui/main.qml:230
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Læsa stærðarhlutföllum"

#: ui/main.qml:238
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: ui/main.qml:246
#, kde-format
msgid "Tool Button 1"
msgstr "Pennahnappur 1"

#: ui/main.qml:247
#, kde-format
msgid "Tool Button 2"
msgstr "Pennahnappur 2"

#: ui/main.qml:248
#, kde-format
msgid "Tool Button 3"
msgstr "Pennahnappur 3"

#: ui/main.qml:291
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Teikniborðsbakki:"

#: ui/main.qml:302
#, kde-format
msgid "None"
msgstr "Ekkert"

#: ui/main.qml:324
#, kde-format
msgid "Button %1:"
msgstr "Hnappur %1:"
